import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    map: {
      showing: true
    },
    lands: [
      {
        name: "Los'lantis",
        key: "loslantis",
        active: true,
        hovering: false,
        showing: false,
        highlighted: false,
        description:
          "This is the underwater city with a portion of it being a breathable bubble filled with neon lights and casinos. The city is made a tourist location by the {Sorcerer of Mardock} who helped cast the bubble spell to make a breathable bubble underwater."
      }
      // {
      //   name: "Altador",
      //   key: "altador",
      //   active: true,
      //   hovering: false,
      //   showing: false,
      //   highlighted: false
      // }
    ],
    characters: [
      {
        name: "Captain Hammerhead",
        key: "hammerhead",
        active: true,
        description:
          "Head of the guard in Los'lantis. Old war veteran. Real name is Sean ..."
      }
    ]
  },
  mutations: {
    showMap(state) {
      state.map.showing = true;
    },
    hideMap(state) {
      state.map.showing = false;
    },
    isLandHovering(state, landKey) {
      var land = state.lands.find(land => land.key === landKey);
      land.hovering = true;
    },
    notLandHovering(state, landKey) {
      var land = state.lands.find(land => land.key === landKey);
      land.hovering = false;
    },
    highlightLand(state, landKey) {
      state.lands.forEach(land => {
        land.highlighted = land.key === landKey;
      });
    },
    showLand(state, landKey) {
      var land = state.lands.find(land => land.key === landKey);
      land.showing = true;
      state.lands.forEach(land => {
        land.highlighted = false;
      });
    },
    hideLand(state, landKey) {
      var land = state.lands.find(land => land.key === landKey);
      land.showing = false;
      state.lands.forEach(land => {
        land.highlighted = false;
      });
    }
  },
  actions: {
    showMap(context) {
      context.commit("showMap");
    },
    hideMap(context) {
      context.commit("hideMap");
    },
    toggleMap(context) {
      var map = context.state.map;
      if (map.showing) {
        context.commit("hideMap");
      } else {
        context.commit("showMap");
      }
    },
    toggleLandHover(context, landKey) {
      var land = context.state.lands.find(land => land.key === landKey);
      if (land.hovering) {
        context.commit("notLandHovering", landKey);
      } else {
        context.commit("isLandHovering", landKey);
      }
    },
    clickLandOnMap(context, landKey) {
      context.commit("showLand", landKey);
      context.commit("highlightLand", landKey);
    },
    toggleLandShowing(context, landKey) {
      var land = context.state.lands.find(land => land.key === landKey);
      if (land.showing) {
        context.commit("hideLand", landKey);
      } else {
        context.commit("showLand", landKey);
      }
    }
  },
  modules: {}
});
